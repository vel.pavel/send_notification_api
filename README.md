# Сервис уведомлений
Cервис управления рассылками API администрирования и получения статистики. Доступ через API и через WEB интерфейс.

## Обзор:
### Worflow
* Завести в сервис клиентов - кому рассылать сообщения
* Завести правила рассылок - время когда отправлять и набор фильтров для выбора группы клиентов
* В заданное время запустится рассылка и через внешний сервис https://probe.fbrq.cloud/docs отпарвит сообщения
* (опционально) Посмотреть статистику отправлений. Сколько сообщений планировалось, сколько отправилось.

### Основные сущности
#### Клиент
* Телефон - номер телефона в формате 7DDDDDDDDDD (где D - это цифры 0-9).
* Код мобильного оператора - 3 цифры шифрующие информацию о мобильном операторе. По умолчанию автоматически берутся первые 3 цифры после 7. Однако вспомним, что сейчас рарешено менять мобильного оператора с сохранением номера. И код может указывать допустим на МТС, а реальный оператор Тинькоф. Если вы знаете "приафльный" код, то можете его указать.
* Тег (опционально)  - произвольная метка к клиенту. Нужна дял сегментации пользователей. Обратите внимание, что на текущий момоент у клиента может быть только одна метка.
* Часовой пояс. Часовой пояс в формате 	Europe/Moscow. Можете посмотреть список https://en.wikipedia.org/wiki/List_of_tz_database_time_zones. Учитывая, что номера с 7ки, по умолчанию равен Московскому.

#### Рассылка
* Дата начала рассылки - дата и время когда должна начаться рассылка.
* Дата окончания рассылки - дата и время когда рассылка станет не актуальной. Всё что не успело отправится до этого времени и не отправится.
* Текст сообщения. Что шлём.
* Фильтр по тегу. (опционально) Если задан, то для рассылки будут выбраны только клиенты с таким же тегом.
* Фильтр по коду оператора (опционально). Если задан, то для рассылки будут выбраны только клиенты с таким же кодом оператора. Фильтры применяются через логическое И. Т.е. если заданы И тег И оператор, то должны выполниться оба условия.
* Время начала и время окончания рассылки. Предположим вы не хотите ночью беспокоить клиентов. Но часовых поясов много. Вот ту вам и пригодятся эти параметры. Сообщение будет отпарвлено только если локальное время клиента (на основание заданного в его настройках часового пояса) попадает в указанный здесь интервал. Обратите внимание, что можно содать и наборот ночную рассылку, указав время начла вечер, а время окончания - утро.

#### Сообщение
* Клиент - кому отправили
* Рассылка - в рамках какой рассылки
* Статус - доставлено, ждёт, отмено, повторная отправка
* Время отпарвки - когда было отправлено для отправленных сообщений

### Статистика
#### Сводная.
По каждой рассылке доступна информация была ли рассылка запущена. И если была, то сколько сообщений планировалось отправить, а сколько отправлено.
#### Детальная
По каждой рассылке можно получить список всех сообщений с их статусами.

### WEB интерфейс.
Для зарегистрированных пользователей доступна панель управления в которой можно просматривать, создавать, редактировать и удалять клиентов и рассылки. А так же получать статистику и сообщения.

### Надёжность отправки.
Отправка ведётся через сторонний сервис, который может быть не доступен, не отвечать и т.п. В случае если сообщение не удалось отправить, то наша система с упорством будет повторять попытки, пока не истечёт время рассылки.

## Установка проекта

### Окружение проекта:
  * python 3.8
  * Django 2.2.28
  * djangorestframework==3.13.1
  * Celery
  * Redis

### Развёртывание
#### Установка через docker (рекомендуется)
* Установите docker

* Склонируйте репозиторий с помощью git
 ```bash
git clone https://gitlab.com/vel.pavel/send_notification_api.git
```

* Перейдите в папку проекта:
```bash
cd send_notification_api
```

* Заполните данные в файле .env. Минимум для работы:
  * SEND_MESSAGE_KEY - ваш токен к сервису отправления сообщений (токен от https://probe.fbrq.cloud/v1/send/).
  * Настройки сервера отправки email (хост, логин, пароль, порт)
  * EMAIL_TO_STATS - кому отправлять статистику

* Запуск и сборка контейнеров:
```bash
sudo docker-compose up -d --build
```
**Готово**
* http://localhost:8000/docs/ - документация
* http://localhost:8000/notifications/api/ - api
* http://localhost:8000/admin/ - админка
* http://localhost:5555/ flower

В контейнерах PostgresSQL, джанго приложение, отдельные контейнеры под Celery и Flower. В процессе развёртывания создаётся база данных, применяются миграции, создётся суперпользователь. 

#### Установка вручную
* Установите redis, если он не установлен в вашей системе. https://redis.io/docs/getting-started/installation/
```bash
sudo apt-get update
sudo apt-get install redis
```
* Установите PostgreSQL или предпочитаемую вами СУБД. Созадйте в ней БД, пользователя.  

* Склонируйте репозиторий с помощью git
 ```bash
git clone https://gitlab.com/vel.pavel/send_notification_api.git
```

* Перейдите в папку проекта:
```bash
cd send_notification_api
```

* Заполните данные в файле .env Обязательно указать:
  * SEND_MESSAGE_KEY - ваш токен к сервису отправления сообщений (токен от https://probe.fbrq.cloud/v1/send/).
  * Настройки сервера отправки email (хост, логин, пароль, порт)
  * EMAIL_TO_STATS - кому отправлять статистику
  * Настройки базы данных. Хост, база, парт, юзер
  * Настройки redis - хост, порт

* Создайте и активируйте виртуальное окружение Python.
```bash
python -m venv venv  
source venv/bin/activate
```

* Установите зависимости из файла **requirements.txt**:
```bash
pip install -r requirements.txt
```

### Конфигуруирование:
* Создайте миграции и примените для БД
```bash
python manage.py makemigrations
python manage.py migrate
```
* Создайте суперпользователя (потребуется ввести имя пользователя и пароль. По желанию email)
```bash
python manage.py createsuperuser
```

### Запуск в тестовом режиме
* Запускаем Celery
```bash
celery -A sendNotification worker -B -l info
```
* Запускаем тестовый сервер
```bash
python manage.py runserver 0:8000
```
По желанию можно зпустить flower для мониторинга celery
```bash
celery -A sendNotification flower
```
Веб морда мониторинга будет на http://localhost:5555
**Готово! Можно попробовать!**

### Возможные проблемы и их решения
* Выдаёт ошибку об отсуствии venv
```bash
apt install python3.8-venv
```
* Все таски celery висят в pending. Добавьте ключ  -P solo https://github.com/celery/celery/issues/6386
```bash
celery -A sendNotification worker -B -l info -P solo
```
## Основные страницы
* Документация API в формате Swagger /docs/
* Документация API в формате redoc /docs/redoc/
* Скачать документацию API в формате YAML /docs/swagger.yaml
* Админка для управления рассылками /admin/

## API
Детально ознакомиться с API можно в документации (ссылки выше) или посмотрев файл swagger.yaml из репозитория.
На текущий момент все методы не требуют авторизации - не открывайте их наружу.
### Основные endpoints
#### Клиенты
##### notifications/api/Clients/
* Get - список и информация по всем клиентам
* Post - Добавление нового клиента
##### notifications/api/Clients/{id}/
* Get - информация о клиенте с указанном id
* Put - изменение клиента
* Delete - удаление клиента
#### Рассылки
##### notifications/api/Mailing/
* Get - список и информация по всем рассылкам
* Post - Добавление новой рассылки
##### notifications/api/Mailing/{id}/
* Get - информация о рассылке с указанным id
* Put - изменение рассылки
* Delete - удаление рассылки
#### Статистика
##### notifications/api/Stats/
* Get - информация по весм рассылкам сколько планировалось сообщений, сколько отправлено
##### notifications/api/Stats/{id}/
* Get - все сообщения с их статусами для рассылки с указанным id

## Хотите предложить что-то поменять?
### Бизнес логику
Предварительно ознакомтьесь с
* ТЗ https://www.craft.do/s/n6OVYFVUpq0o6L
* Списком предварительных вопросов к PM/аналитику или иному автору ТЗ ToPMorBA.txt. Данный проект разрабатывался в режиме симуляции рядового разработчика, а не PM/Teamlead и без доступа к заказчику. С ограничениями в сжатые сроки (3-4 часа заявлено, на эту доку ушло больше часа :) ). Помните, что всякая супер странная логика может быть именно то, что нужно заказчику. 
### Код взаимодйествия с тасками
На всякий случай посмотрите вначале файл notifications/CeleryTasksLogik.txt - там немного пяоснения почему сейчас именно так и несколько кейсов.

## Критерии приёмки
### Основные
* Выполненное задание необходимо разместить в публичном репозитории на gitlab.com  **done**
* Понятная документация по запуску проекта со всеми его зависимостями **done** Выше в этом документе
* Документация по API для интеграции с разработанным сервисом и Описание реализованных методов в формате OpenAPI **done** кратко в этой доке, полнее документации swagger и redoc в проекте +yaml файл
### Дополнительные
* 5 сделать так, чтобы по адресу /docs/ открывалась страница со Swagger UI и в нём отображалось описание разработанного API. **done**
* 6 реализовать администраторский Web UI для управления рассылками и получения статистики по отправленным сообщениям реализовать администраторский Web UI для управления рассылками и получения статистики по отправленным сообщениям **done** /admin/ Управление. Сводная статистика по рассылкам выведена в список рассылок.
* 8 реализовать дополнительный сервис, который раз в сутки отправляет статистику по обработанным рассылкам на email. **done** Раз в день отпарвляется сттистика.
* 9 удаленный сервис может быть недоступен, долго отвечать на запросы или выдавать некорректные ответы. Необходимо организовать обработку ошибок и откладывание запросов при неуспехе для последующей повторной отправки. Задержки в работе внешнего сервиса никак не должны оказывать влияние на работу сервиса рассылок. **done** В случае ошибок задача перезаупскается до победного или истечения строка жизни рассылки.
* 11 реализовать дополнительную бизнес-логику: добавить в сущность "рассылка" поле "временной интервал", в котором можно задать промежуток времени, в котором клиентам можно отправлять сообщения с учётом их локального времени. Не отправлять клиенту сообщение, если его локальное время не входит в указанный интервал. **done** Так же предусмотрены случаи если введена только одн из границ интервала и случаи когда время начала больше, чем время конца (ночная рассылка)
* 12 Обеспечить подробное логирование на всех этапах обработки запросов, чтобы при эксплуатации была возможность найти в логах всю информацию **notDone** на текущий момент только логирование выполнения задач. Да ещё не в очень удобном формате.
* 

Часть остальных доп пунктов будет реализована позже.