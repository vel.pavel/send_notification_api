from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import action
from django.shortcuts import render, get_object_or_404

from .serializers import ClientSerializer, MailingSerializer, StatisticMailingList, MessageSerializer
from .models import Client, Mailing, Message


class ClientViewSet(viewsets.ModelViewSet) :
    """
    retrieve:
        Return a client.

    list:
        Return all clients.

    create:
        Create a new client.

    delete:
        Remove an existing client.
    """
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class MailingSerializer(viewsets.ModelViewSet) :
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer


class StatsListSerializer(viewsets.ReadOnlyModelViewSet) :
    """
        retrieve:
            Return all messages for one mailing.

        list:
            Return all mailing with info about planed and sent messages.

    """
    queryset = Mailing.objects.all()
    serializer_class = StatisticMailingList

    # @action(detail=True, methods=['get'])
    def retrieve(self, request, pk=None) :
        get_object_or_404(Mailing.objects.all(), pk=pk)
        queryset = Message.objects.filter(mailing=pk).all()
        serializer = MessageSerializer(queryset, many=True)
        return Response(serializer.data)

# class StatsList(APIView):
#     def get(self, request, format=None):
#         queryset = Mailing.objects.all()
#         serializer = StatisticMailingList(queryset, many=True)
#         return Response(serializer.data)
#
#
# class StatsDetail(APIView):
#     def get(self, request, pk, format=None):
#         #get_object_or_404(Mailing.objects.all(), pk=pk)
#         queryset = Message.objects.filter(mailing=pk).all()
#         serializer = MessageSerializer(queryset, many=True)
#         return Response(serializer.data)
