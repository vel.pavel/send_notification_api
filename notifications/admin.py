from django.contrib import admin
from .models import Mailing, Client, Message


class MailingAdmin(admin.ModelAdmin):
    list_display = ('id', 'start_datetime', 'end_datetime', 'start_time', 'end_time', 'processed', 'all_messages', 'sent_messages')

class MessageAdmin(admin.ModelAdmin):
    list_display = ('id', 'status', 'mailing', 'client')
    list_filter = ['status']

class ClientAdmin(admin.ModelAdmin) :
    list_display = ('id', 'phone_number', 'mobile_operator_code', 'tz')


admin.site.register(Mailing, MailingAdmin)
admin.site.register(Message, MessageAdmin)
admin.site.register(Client, ClientAdmin)
